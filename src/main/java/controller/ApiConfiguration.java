package controller;

import java.io.FileInputStream;
import java.util.Properties;

import helpers.ApacheSparkSettings;
import lombok.Getter;

public class ApiConfiguration {
	private static ApiConfiguration _instance = null;
	private @Getter Integer port = 5005;	
	private @Getter ApacheSparkSettings apacheSparkSettings = null;
	
	protected ApiConfiguration() {
		try {
			Properties props = new Properties();
			
			apacheSparkSettings = new ApacheSparkSettings();
			apacheSparkSettings.setMaster(props.getProperty("spark.master"));
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	static public ApiConfiguration instance() {
		if (_instance == null) {
			_instance = new ApiConfiguration();
		}
		return _instance;
	}
}
