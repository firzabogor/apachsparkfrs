package controller;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.OptionalInt;
import java.util.Properties;
import java.util.stream.Collectors;

import org.apache.spark.SparkConf;
import org.apache.spark.SparkContext;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.api.java.UDF1;
import org.apache.spark.sql.api.java.UDF2;
import org.apache.spark.sql.api.java.UDF3;
import org.apache.spark.sql.api.java.UDF4;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructType;
import org.eclipse.jetty.websocket.common.frames.DataFrame;
import org.apache.spark.sql.functions;
import org.apache.spark.sql.expressions.Window;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import helpers.ApacheSparkHelper;
import helpers.ServiceHelper;
import io.netty.handler.codec.http.HttpContentEncoder.Result;
import lombok.var;
import models.Minimum;
import scala.annotation.meta.field;
import scala.collection.Seq;
import scala.collection.immutable.Stream;
import scala.collection.mutable.WrappedArray;
import services.FrsService;
import spark.Request;
import spark.Response;
import spark.Route;

public class FrsController {
	
	public static Route live = (Request request, Response response) -> {
		response.type("application/json");
		response.header("Access-Control-Allow-Origin", "*");
		
		var conf = ApacheSparkHelper.getConfiguration();
		var spark = SparkSession.builder().config(conf).getOrCreate();
		var url = "jdbc:sqlserver://localhost:1433";
		var table = "dbo.area_provinsi";
		
		var properties = new Properties();
		properties.put("user","sa");
		properties.put("password","Blackrose9");
		
		
		Dataset<Row> jdbcDF = spark.read().jdbc(url, table, properties);
		
		jdbcDF.show();
		
		return "live";
	};
	
	public static Route dataseting = (Request request, Response response) -> {
		response.type("application/json");
		response.header("Access-Control-Allow-Origin", "*");
		
		String params = request.body();
		
		JsonParser parser = new JsonParser();
		JsonElement paramsJson = parser.parse(params);
		
		JsonObject paramsObject = paramsJson.getAsJsonObject();
		String dataset = paramsObject.get("dataset").getAsString();
		Boolean headers = paramsObject.get("headers").getAsBoolean();

		
		return "testing";
	};
	
	public static Route testing = (Request request, Response response) -> {
		response.type("application/json");
		response.header("Access-Control-Allow-Origin", "*");
		
		String params = request.body();
		
		JsonParser parser = new JsonParser();
		JsonElement paramsJson = parser.parse(params);
		
		JsonObject paramsObject = paramsJson.getAsJsonObject();
		String dataset = paramsObject.get("dataset").getAsString();
		Boolean headers = paramsObject.get("headers").getAsBoolean();
		
		if(headers.equals(true)) {
			System.out.println("only get a header");
		} else {
			System.out.println("get the whole of dataset");
		}
		
		
		BufferedReader br = null;
		String result = null;
		try {
			if(dataset.equals("dataset")) {
				br = new BufferedReader(new FileReader("C:/Users/62856/Desktop/data/data.json"));
			}
			else if(dataset.equals("dataset2")) {
				br = new BufferedReader(new FileReader("C:/Users/62856/Desktop/data/data2.json"));
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
		
		return br.lines().collect(Collectors.joining());
	};
	
	public static Route stratifikasi = (Request request, Response response) -> {
		response.type("application/json");
		response.header("Access-Control-Allow-Origin", "*");
		
		var conf = ApacheSparkHelper.getConfiguration();
		var spark = SparkSession.builder().config(conf).getOrCreate();
		spark.conf().set("spark.sql.debug.maxToStringFields", 1000);
		
		/* --------------------------STRATIFIKASI AWAL----------------------------- */
		
		var path = "C:/Users/62856/Desktop/SHOPI/original/bs_stra.csv";
		Dataset<Row> dataset = spark.read().format("csv").option("inferSchema", "true").option("header", "true").load(path);
		
		//bs_gabung_match_mfd_ok
		dataset.createTempView("bs_gabung_match_mfd_ok");
		
		String alias = "bs_stra";
		
		String[] commodity_item = {"Bawang Merah", "Bawang Putih", "Cabai Besar", "Cabai Rawit"};
		String[] commodity_code = {"204","206","211","215"};

		Integer commodity = commodity_item.length;
		
		Column[] data = new Column[commodity];
		for(int i=0; i<commodity; i++) {
			int number = i+1;
			String colname = "jrt_kom"+number;
			data[i] = dataset.col(colname);
		}
	
		for(int i=1; i<=commodity; i++) {
			dataset = dataset.withColumn("pr_kom"+i, functions.when(functions.greatest(data).equalTo(dataset.col("jrt_kom"+i)), 1).otherwise(0));
		}
		
		String greatest = "GREATEST(";
		for(int i=1; i<=commodity; i++) {
			if(i==1) {
				greatest += "jrt_kom"+i;
			} else {
				greatest += ",jrt_kom"+i;
			}
		}
		greatest += ")";
		
		String expr = "CASE ";
		for(int i=1; i<=commodity; i++) {
			expr += "WHEN "+greatest+" = "+"jrt_kom"+i+" THEN "+i+" ";
		}
		expr += "END";
		
		dataset = dataset.withColumn("ranking1", functions.expr(expr));
		
		String query = "SELECT *,(";
		for(int i=1; i<=commodity; i++) {
			if(i == 1) {
				query += "pr_kom"+i;
			} else {
				query += "+pr_kom"+i;
			}
		}
		query += ") AS flag_sama FROM "+alias;
		
		dataset.createTempView(alias);
		
		dataset = spark.sql(query);
		
		dataset = dataset.withColumn("ranking1", functions.when(dataset.col("flag_sama").notEqual(1), 0).otherwise(dataset.col("ranking1")));
		
		String expr2 = "CASE WHEN ranking1 = 0 THEN '' ";
		for(int i=0; i<commodity; i++) {
			int number = i+1;
			expr2 += "WHEN ranking1 = "+number+ " THEN '"+ commodity_item[i] +"' ";
		}
		expr2 += "END";
		
		dataset = dataset.withColumn("str_awal", functions.expr(expr2));
		
		String alias2 = "strata_awal";
		dataset.createTempView(alias2);
		
		/* ------------------------------------------------------------------- */
		
		/* --------------------------PECAH BLOK SENSUS PER KABUPATEN----------------------------- */

		String query2 = "SELECT LEFT(bs18_1,2) AS pr, SUBSTR(bs18_1,3,2) AS kb FROM "+alias2+ " GROUP BY pr,kb";
		var master_kabupaten = spark.sql(query2);
		
		master_kabupaten = master_kabupaten.select("pr","kb").distinct();
		master_kabupaten.createTempView("master_kabupaten");
		
		String query3 = "SELECT prop,kab,";
		for(int i=1; i<=commodity;i++) {
			if(i == 1) {
				query3 += "SUM(jrt_kom"+i+") AS sum_j"+i+" ";
			} else {
				query3 += ",SUM(jrt_kom"+i+") AS sum_j"+i+" ";
			}
		}
		query3 += "FROM "+alias2+" GROUP BY prop,kab";
		
		var jrt_kom = spark.sql(query3);
		jrt_kom.createTempView("jrt_kom");
		
		String query4 = "SELECT a.*,";
		for(int i=1; i<=commodity;i++) {
			if(i == 1) {
				query4 += "CASE WHEN a.flag_sama>1 THEN b.sum_j"+i+" ELSE NULL END AS sum_j"+i+" ";
			} else {
				query4 += ",CASE WHEN a.flag_sama>1 THEN b.sum_j"+i+" ELSE NULL END AS sum_j"+i+" ";
			}
		}
		
		query4 += "FROM strata_awal a LEFT JOIN jrt_kom b ON (a.kab=b.kab AND a.prop=b.prop)";
		
		dataset = spark.sql(query4);
		dataset.createTempView("j_str");
		
		var query5 = "SELECT prop,kab,";
		for(int i=0; i<commodity;i++) {
			int number = i+1;
			if(i == 0) {
				query5 += "SUM(CASE WHEN str_awal='"+commodity_item[i]+"' THEN 1 ELSE 0 END) AS j_str"+number+" ";
			} else {
				query5 += ",SUM(CASE WHEN str_awal='"+commodity_item[i]+"' THEN 1 ELSE 0 END) AS j_str"+number+" ";
			}
		}
		
		query5 += "FROM j_str GROUP BY prop,kab";
		
		var jrt_kom_group = spark.sql(query5);
		jrt_kom_group.createTempView("jrt_kom_group");
		
		String query6 = "SELECT a.*,";
		for(int i=1; i<=commodity;i++) {
			if(i == 1) {
				query6 += "CASE WHEN a.flag_sama>1 THEN b.j_str"+i+" ELSE NULL END AS j_str"+i+" ";
			} else {
				query6 += ",CASE WHEN a.flag_sama>1 THEN b.j_str"+i+" ELSE NULL END AS j_str"+i+" ";
			}
		}
		
		query6 += "FROM j_str a LEFT JOIN jrt_kom_group b ON (a.kab=b.kab AND a.prop=b.prop)";
		
		dataset = spark.sql(query6);
		dataset.createTempView("pilih_kom");
		
		//dataset.where("prop == 13").where("kab == 03").show(1000);
		

		//dataset = dataset.where("str_awal == 'Cabai Rawit'").where("prop == 13").where("kab == 03");
		//dataset.groupBy("str_awal").count().show();
		
		/*-----------------------------TAHAP 7 PROGRAM--------------------------------*/
		var path2 = "C:/Users/62856/Desktop/SHOPI/csv/Frame_strata_awal_gabung.csv";
		var dataset2 = spark.read().format("csv").option("inferSchema", "true").option("header", "true").load(path2);
		dataset2.createTempView("strata_final");
		
		var query14 = "SELECT prop, kab, bs18_1, nmprop, nmkab, nmkec, nmdesa, klas, idsp10c1, nbsc1, kd_mtd, nks_all, f_st2013_l, f_st2013_p, propst13, kabst13, kecst13, desast13, nbsst13, jrt_eli, jrt_jkom1, jrt_jkom2, f_bs_min10, flag_sama, ranking1, str_awal";
		for(int i=1; i<=commodity;i++) {
			query14 += ",jrt_kom"+i + ",pr_kom"+i ;
		}
		query14 += " FROM strata_final";
		dataset2 = spark.sql(query14);
		
		String expr3 = "CASE ";
		for(int i=0; i<commodity; i++) {
			var number = i+1;
			expr3 += "WHEN ranking1 = "+ number + " THEN "+ commodity_code[i] + " ";
		}
		expr3 += "END";
		
		dataset2 = dataset2.withColumn("kd_str_awl", functions.expr(expr3));
		dataset2.createTempView("jrt_el_commodity");
		
		String query15 ="SELECT *, (";
		for(int i=1; i<=commodity; i++) {
			if(i==1) {
				query15 += "jrt_kom"+i;
			} else {
				query15 += "+jrt_kom"+i;
			}
		}
		query15 += ") AS jrt_el_kom FROM jrt_el_commodity";
		
		dataset2 = spark.sql(query15);
		
		//frame_strata_awal_gabung_olah
		dataset2.createTempView("jrt_el");
		
		String query_mst1 = "SELECT prop, kab, kd_str_awl, str_awal FROM jrt_el GROUP BY prop, kab, str_awal, kd_str_awl";
		String query_jbs1 = "SELECT prop, kab, kd_str_awl, str_awal, COUNT(*) as jml_bs FROM jrt_el GROUP BY prop, kab, str_awal, kd_str_awl";
		String query_jrtkom1 = "SELECT prop, kab, kd_str_awl, str_awal, SUM(jrt_el_kom) as jrt_kom FROM jrt_el GROUP BY prop, kab, str_awal, kd_str_awl";
		
		spark.sql(query_mst1).createTempView("mst1");
		spark.sql(query_jbs1).createTempView("jbs1");
		spark.sql(query_jrtkom1).createTempView("jrtkom1");
		
		var rekap_stra = spark.sql("SELECT a.*, b.jml_bs, c.jrt_kom FROM mst1 AS a LEFT JOIN jbs1 AS b ON a.prop+a.kab+a.kd_str_awl=b.prop+b.kab+b.kd_str_awl LEFT JOIN jrtkom1 as c ON a.prop+a.kab+a.kd_str_awl=c.prop+c.kab+c.kd_str_awl");
		rekap_stra = rekap_stra.sort(functions.asc("prop"), functions.asc("kab"), functions.desc("jrt_kom"));
		
		String[] column = rekap_stra.columns();
		rekap_stra = rekap_stra.withColumn("No", functions.row_number().over(Window.orderBy(column[0].toString())));
		rekap_stra = rekap_stra.withColumn("prop_kab", functions.concat(rekap_stra.col("prop"),rekap_stra.col("kab")));
		rekap_stra.createTempView("rekap_stra");
		
		var total_jrt = spark.sql("SELECT prop_kab,SUM(jrt_kom) AS Total FROM rekap_stra GROUP BY prop_kab");
		total_jrt.createTempView("total_jrt");
		
		var kum_size = spark.sql("SELECT *, SUM(jrt_kom) OVER (PARTITION BY prop_kab ORDER BY No) AS kum_size FROM rekap_stra");
		kum_size.createTempView("kum_size");
		
		var share_rt = spark.sql("SELECT a.*,b.total FROM kum_size a LEFT JOIN total_jrt b ON a.prop_kab = b.prop_kab");
		share_rt.createTempView("share_rt");
		
		share_rt = spark.sql("SELECT *, (kum_size/total)*100 AS share_rt FROM share_rt");
		share_rt = share_rt.withColumn("cop", functions.when(share_rt.col("share_rt").$less(95), 1).otherwise(0));
		
		share_rt = share_rt.withColumn("str_final", functions.when(share_rt.col("cop").equalTo(1), share_rt.col("str_awal")).otherwise("HORTIKULTURA LAIN"));
		share_rt = share_rt.withColumn("kd_str_fnl", functions.when(share_rt.col("cop").equalTo(1), share_rt.col("kd_str_awl")).otherwise(299));
		share_rt.createTempView("share_rt2");
		//share_rt.where(share_rt.col("prop").equalTo(73)).where(share_rt.col("kab").equalTo(16)).where(share_rt.col("kd_str_awl").equalTo(204)).show();
		
		String query16 = "SELECT a.*, b.kd_str_fnl, b.str_final FROM jrt_el a RIGHT JOIN share_rt2 b ON a.prop = b.prop AND a.kab = b.kab AND a.kd_str_awl = b.kd_str_awl";
		
		var frame_strata_final = spark.sql(query16);
		
		//frame_strata_final
		frame_strata_final.createTempView("frame_strata_final");
		
		var frame_strata_final_ok = spark.sql("SELECT a.*, b.jrt_el_kom, null as append FROM frame_strata_final a LEFT JOIN jrt_el b ON a.prop = b.prop AND a.kab = b.kab AND a.bs18_1 = b.bs18_1");
		frame_strata_final_ok.createTempView("frame_strata_final_ok");
		
		String[] columns = frame_strata_final_ok.columns();
		var result = frame_strata_final_ok.withColumn("No", functions.row_number().over(Window.orderBy(columns[0].toString())));
		
		Long total = result.count();
		
		String params = request.body();
		
		JsonParser parser = new JsonParser();
		JsonElement paramsJson = parser.parse(params);
		
		JsonObject paramsObject = paramsJson.getAsJsonObject();
		Integer draw = paramsObject.get("draw").getAsInt();
		Integer length = paramsObject.get("length").getAsInt();
		Integer start = paramsObject.get("start").getAsInt();

		String condition = "No >" + start;
		result = result.where(condition);
		result = result.limit(length);

		List<String> paginatedResults = result.toJSON().collectAsList();
		
		var bs = spark.sql("SELECT a.*, b.sulit18_1 FROM frame_strata_final a LEFT JOIN bs_gabung_match_mfd_ok b ON a.bs18_1 = b.bs18_1");
		bs.createTempView("bs");
		
		var bs_eli = bs.where(bs.col("sulit18_1").notEqual(1));
		bs_eli.createTempView("bs_eli");
		
		var rekap1 = spark.sql("SELECT prop AS kd_prov, nmprop, kab AS kd_kab, kd_str_fnl, str_final, COUNT(*) AS jml_bs, SUM(jrt_eli) as jrt_horti, SUM(f_bs_min10) as jbs_min10 FROM bs GROUP BY kd_prov, nmprop, kd_kab, nmkab, kd_str_fnl, str_final");
		rekap1.createTempView("rekap1");
		
		rekap1.show();
		
		var rekap2 = spark.sql("SELECT prop AS kd_prov, nmprop, kab AS kd_kab, nmkab, kd_str_fnl, str_final, SUM(jrt_eli) AS jrt_horti, SUM(jrt_kom3) AS jrt_cb, SUM(jrt_kom4) AS jrt_cr, SUM(jrt_kom1) AS jrt_bm, SUM(jrt_kom2) AS jrt_bp, SUM(jrt_jkom1) AS jrt_jkom1, SUM(jrt_jkom2) AS jrt_jkom2 FROM bs WHERE f_bs_min10 = 1 GROUP BY kd_prov, nmprop, kd_kab, nmkab, kd_str_fnl, str_final");
		rekap2.createTempView("rekap2");
		
		rekap2.show();
		
		var rekap_sd7 = spark.sql("SELECT prop as kd_prov, nmprop, kab as kd_kab, nmkab, kd_str_fnl, str_final, COUNT(*) as jml_bs, SUM(jrt_eli) as jrt_horti, SUM(jrt_kom3) as jrt_cb, SUM(jrt_kom4) as jrt_cr, SUM(jrt_kom1) as jrt_bm, SUM(jrt_kom2) as jrt_bp, SUM(jrt_jkom1) as jrt_jkom1, SUM(jrt_jkom2) as jrt_jkom2 FROM bs WHERE jrt_eli<7 GROUP BY kd_prov, nmprop, kd_kab, nmkab, kd_str_fnl, str_final");
		rekap_sd7.createTempView("rekap_sd7");	
		
		rekap_sd7.show();
		
		//var frame_strata_final_ok = frame_strata_final.withColumn("append", functions.lit(null));
		spark.stop();
		
		return "{" + 
				"\"draw\":" + draw + 
				",\"recordsTotal\":" + total +
				",\"recordsFiltered\":" + total +
				",\"data\":" + paginatedResults +
				"}";
	};
	
	public static Route stratifikasi_save = (Request request, Response response) -> {
		response.type("application/json");
		response.header("Access-Control-Allow-Origin", "*");
		
		var conf = ApacheSparkHelper.getConfiguration();
		var spark = SparkSession.builder().config(conf).getOrCreate();
		spark.conf().set("spark.sql.debug.maxToStringFields", 1000);
		
		/* --------------------------STRATIFIKASI AWAL----------------------------- */
		
		var path = "C:/Users/62856/Desktop/SHOPI/original/bs_stra.csv";
		Dataset<Row> dataset = spark.read().format("csv").option("inferSchema", "true").option("header", "true").load(path);
		
		//bs_gabung_match_mfd_ok
		dataset.createTempView("bs_gabung_match_mfd_ok");
		
		String alias = "bs_stra";
		
		String[] commodity_item = {"Bawang Merah", "Bawang Putih", "Cabai Besar", "Cabai Rawit"};
		String[] commodity_code = {"204","206","211","215"};

		Integer commodity = commodity_item.length;
		
		Column[] data = new Column[commodity];
		for(int i=0; i<commodity; i++) {
			int number = i+1;
			String colname = "jrt_kom"+number;
			data[i] = dataset.col(colname);
		}
	
		for(int i=1; i<=commodity; i++) {
			dataset = dataset.withColumn("pr_kom"+i, functions.when(functions.greatest(data).equalTo(dataset.col("jrt_kom"+i)), 1).otherwise(0));
		}
		
		String greatest = "GREATEST(";
		for(int i=1; i<=commodity; i++) {
			if(i==1) {
				greatest += "jrt_kom"+i;
			} else {
				greatest += ",jrt_kom"+i;
			}
		}
		greatest += ")";
		
		String expr = "CASE ";
		for(int i=1; i<=commodity; i++) {
			expr += "WHEN "+greatest+" = "+"jrt_kom"+i+" THEN "+i+" ";
		}
		expr += "END";
		
		dataset = dataset.withColumn("ranking1", functions.expr(expr));
		
		String query = "SELECT *,(";
		for(int i=1; i<=commodity; i++) {
			if(i == 1) {
				query += "pr_kom"+i;
			} else {
				query += "+pr_kom"+i;
			}
		}
		query += ") AS flag_sama FROM "+alias;
		
		dataset.createTempView(alias);
		
		dataset = spark.sql(query);
		
		dataset = dataset.withColumn("ranking1", functions.when(dataset.col("flag_sama").notEqual(1), 0).otherwise(dataset.col("ranking1")));
		
		String expr2 = "CASE WHEN ranking1 = 0 THEN '' ";
		for(int i=0; i<commodity; i++) {
			int number = i+1;
			expr2 += "WHEN ranking1 = "+number+ " THEN '"+ commodity_item[i] +"' ";
		}
		expr2 += "END";
		
		dataset = dataset.withColumn("str_awal", functions.expr(expr2));
		
		String alias2 = "strata_awal";
		dataset.createTempView(alias2);
		
		/* ------------------------------------------------------------------- */
		
		/* --------------------------PECAH BLOK SENSUS PER KABUPATEN----------------------------- */

		String query2 = "SELECT LEFT(bs18_1,2) AS pr, SUBSTR(bs18_1,3,2) AS kb FROM "+alias2+ " GROUP BY pr,kb";
		var master_kabupaten = spark.sql(query2);
		
		master_kabupaten = master_kabupaten.select("pr","kb").distinct();
		master_kabupaten.createTempView("master_kabupaten");
		
		String query3 = "SELECT prop,kab,";
		for(int i=1; i<=commodity;i++) {
			if(i == 1) {
				query3 += "SUM(jrt_kom"+i+") AS sum_j"+i+" ";
			} else {
				query3 += ",SUM(jrt_kom"+i+") AS sum_j"+i+" ";
			}
		}
		query3 += "FROM "+alias2+" GROUP BY prop,kab";
		
		var jrt_kom = spark.sql(query3);
		jrt_kom.createTempView("jrt_kom");
		
		String query4 = "SELECT a.*,";
		for(int i=1; i<=commodity;i++) {
			if(i == 1) {
				query4 += "CASE WHEN a.flag_sama>1 THEN b.sum_j"+i+" ELSE NULL END AS sum_j"+i+" ";
			} else {
				query4 += ",CASE WHEN a.flag_sama>1 THEN b.sum_j"+i+" ELSE NULL END AS sum_j"+i+" ";
			}
		}
		
		query4 += "FROM strata_awal a LEFT JOIN jrt_kom b ON (a.kab=b.kab AND a.prop=b.prop)";
		
		dataset = spark.sql(query4);
		dataset.createTempView("j_str");
		
		var query5 = "SELECT prop,kab,";
		for(int i=0; i<commodity;i++) {
			int number = i+1;
			if(i == 0) {
				query5 += "SUM(CASE WHEN str_awal='"+commodity_item[i]+"' THEN 1 ELSE 0 END) AS j_str"+number+" ";
			} else {
				query5 += ",SUM(CASE WHEN str_awal='"+commodity_item[i]+"' THEN 1 ELSE 0 END) AS j_str"+number+" ";
			}
		}
		
		query5 += "FROM j_str GROUP BY prop,kab";
		
		var jrt_kom_group = spark.sql(query5);
		jrt_kom_group.createTempView("jrt_kom_group");
		
		String query6 = "SELECT a.*,";
		for(int i=1; i<=commodity;i++) {
			if(i == 1) {
				query6 += "CASE WHEN a.flag_sama>1 THEN b.j_str"+i+" ELSE NULL END AS j_str"+i+" ";
			} else {
				query6 += ",CASE WHEN a.flag_sama>1 THEN b.j_str"+i+" ELSE NULL END AS j_str"+i+" ";
			}
		}
		
		query6 += "FROM j_str a LEFT JOIN jrt_kom_group b ON (a.kab=b.kab AND a.prop=b.prop)";
		
		dataset = spark.sql(query6);
		dataset.createTempView("pilih_kom");
		
		//dataset.where("prop == 13").where("kab == 03").show(1000);
		

		//dataset = dataset.where("str_awal == 'Cabai Rawit'").where("prop == 13").where("kab == 03");
		//dataset.groupBy("str_awal").count().show();
		
		/*-----------------------------TAHAP 7 PROGRAM--------------------------------*/
		var path2 = "C:/Users/62856/Desktop/SHOPI/csv/Frame_strata_awal_gabung.csv";
		var dataset2 = spark.read().format("csv").option("inferSchema", "true").option("header", "true").load(path2);
		dataset2.createTempView("strata_final");
		
		var query14 = "SELECT prop, kab, bs18_1, nmprop, nmkab, nmkec, nmdesa, klas, idsp10c1, nbsc1, kd_mtd, nks_all, f_st2013_l, f_st2013_p, propst13, kabst13, kecst13, desast13, nbsst13, jrt_eli, jrt_jkom1, jrt_jkom2, f_bs_min10, flag_sama, ranking1, str_awal";
		for(int i=1; i<=commodity;i++) {
			query14 += ",jrt_kom"+i + ",pr_kom"+i ;
		}
		query14 += " FROM strata_final";
		dataset2 = spark.sql(query14);
		
		String expr3 = "CASE ";
		for(int i=0; i<commodity; i++) {
			var number = i+1;
			expr3 += "WHEN ranking1 = "+ number + " THEN "+ commodity_code[i] + " ";
		}
		expr3 += "END";
		
		dataset2 = dataset2.withColumn("kd_str_awl", functions.expr(expr3));
		dataset2.createTempView("jrt_el_commodity");
		
		String query15 ="SELECT *, (";
		for(int i=1; i<=commodity; i++) {
			if(i==1) {
				query15 += "jrt_kom"+i;
			} else {
				query15 += "+jrt_kom"+i;
			}
		}
		query15 += ") AS jrt_el_kom FROM jrt_el_commodity";
		
		dataset2 = spark.sql(query15);
		
		//frame_strata_awal_gabung_olah
		dataset2.createTempView("jrt_el");
		
		String query_mst1 = "SELECT prop, kab, kd_str_awl, str_awal FROM jrt_el GROUP BY prop, kab, str_awal, kd_str_awl";
		String query_jbs1 = "SELECT prop, kab, kd_str_awl, str_awal, COUNT(*) as jml_bs FROM jrt_el GROUP BY prop, kab, str_awal, kd_str_awl";
		String query_jrtkom1 = "SELECT prop, kab, kd_str_awl, str_awal, SUM(jrt_el_kom) as jrt_kom FROM jrt_el GROUP BY prop, kab, str_awal, kd_str_awl";
		
		spark.sql(query_mst1).createTempView("mst1");
		spark.sql(query_jbs1).createTempView("jbs1");
		spark.sql(query_jrtkom1).createTempView("jrtkom1");
		
		var rekap_stra = spark.sql("SELECT a.*, b.jml_bs, c.jrt_kom FROM mst1 AS a LEFT JOIN jbs1 AS b ON a.prop+a.kab+a.kd_str_awl=b.prop+b.kab+b.kd_str_awl LEFT JOIN jrtkom1 as c ON a.prop+a.kab+a.kd_str_awl=c.prop+c.kab+c.kd_str_awl");
		rekap_stra = rekap_stra.sort(functions.asc("prop"), functions.asc("kab"), functions.desc("jrt_kom"));
		
		String[] column = rekap_stra.columns();
		rekap_stra = rekap_stra.withColumn("No", functions.row_number().over(Window.orderBy(column[0].toString())));
		rekap_stra = rekap_stra.withColumn("prop_kab", functions.concat(rekap_stra.col("prop"),rekap_stra.col("kab")));
		rekap_stra.createTempView("rekap_stra");
		
		var total_jrt = spark.sql("SELECT prop_kab,SUM(jrt_kom) AS Total FROM rekap_stra GROUP BY prop_kab");
		total_jrt.createTempView("total_jrt");
		
		var kum_size = spark.sql("SELECT *, SUM(jrt_kom) OVER (PARTITION BY prop_kab ORDER BY No) AS kum_size FROM rekap_stra");
		kum_size.createTempView("kum_size");
		
		var share_rt = spark.sql("SELECT a.*,b.total FROM kum_size a LEFT JOIN total_jrt b ON a.prop_kab = b.prop_kab");
		share_rt.createTempView("share_rt");
		
		share_rt = spark.sql("SELECT *, (kum_size/total)*100 AS share_rt FROM share_rt");
		share_rt = share_rt.withColumn("cop", functions.when(share_rt.col("share_rt").$less(95), 1).otherwise(0));
		
		share_rt = share_rt.withColumn("str_final", functions.when(share_rt.col("cop").equalTo(1), share_rt.col("str_awal")).otherwise("HORTIKULTURA LAIN"));
		share_rt = share_rt.withColumn("kd_str_fnl", functions.when(share_rt.col("cop").equalTo(1), share_rt.col("kd_str_awl")).otherwise(299));
		share_rt.createTempView("share_rt2");
		//share_rt.where(share_rt.col("prop").equalTo(73)).where(share_rt.col("kab").equalTo(16)).where(share_rt.col("kd_str_awl").equalTo(204)).show();
		
		String query16 = "SELECT a.*, b.kd_str_fnl, b.str_final FROM jrt_el a RIGHT JOIN share_rt2 b ON a.prop = b.prop AND a.kab = b.kab AND a.kd_str_awl = b.kd_str_awl";
		
		var frame_strata_final = spark.sql(query16);
		
		//frame_strata_final
		frame_strata_final.createTempView("frame_strata_final");
		
		var frame_strata_final_ok = spark.sql("SELECT a.*, b.jrt_el_kom, null as append FROM frame_strata_final a LEFT JOIN jrt_el b ON a.prop = b.prop AND a.kab = b.kab AND a.bs18_1 = b.bs18_1");
		
		List<String> paginatedResults = frame_strata_final_ok.limit(50).toJSON().collectAsList();
		
		spark.stop();
		
		return paginatedResults;
	};
	
	public static Route test = (Request request, Response response) -> {
		response.type("application/json");
		response.header("Access-Control-Allow-Origin", "*");
		
		var path = "C:/Users/62856/Desktop/SHOPI/csv/hirarki_match_flag.csv";
		var spark = SparkSession.builder().config(ApacheSparkHelper.getConfiguration()).getOrCreate();
		Dataset<Row> dataset = spark.read().format("csv").option("header", "true").load(path);
		dataset.createTempView("df");
		
		Dataset<Row> result = spark.sql("SELECT * FROM df");
		List<String> jsonResult = result.toJSON().collectAsList();
		
		spark.stop();
		
		
		return "{" + 
					"\"data\":" + jsonResult +
				"}";
	};
	
	public static Route getDataset2 = (Request request, Response response) -> {
		response.type("application/json");
		response.header("Access-Control-Allow-Origin", "*");
		
		String params = request.body();
		
		JsonParser parser = new JsonParser();
		JsonElement paramsJson = parser.parse(params);
		
		JsonObject paramsObject = paramsJson.getAsJsonObject();
		Integer draw = paramsObject.get("draw").getAsInt();
		Integer length = paramsObject.get("length").getAsInt();
		Integer start = paramsObject.get("start").getAsInt();
		JsonArray df = paramsObject.get("dataframe").getAsJsonArray();
		String query = paramsObject.get("query").getAsString();
		
		var spark = SparkSession.builder().config(ApacheSparkHelper.getConfiguration()).getOrCreate();
		
		String path = "";
		for(int i = 0; i<df.size(); i++) {
			JsonObject jsonObject = df.get(i).getAsJsonObject();
			String name = jsonObject.get("dataset").getAsString();
			
			if(name.equals("rekap2018")) {
				path = "C:/Users/62856/Desktop/SHOPI/csv/rekap2018.csv";
			} 
			else if(name.equals("masterBlokSensus")) {
				path = "C:/Users/62856/Desktop/SHOPI/csv/mstbs_20121_st2013_ok.csv";
			} 
			else if(name.equals("hierarkiBlokSensus")) {
				path = "C:/Users/62856/Desktop/SHOPI/csv/hirarki_match_flag.csv";
			}  
			else if(name.equals("blokSensusGabung")) {
				path = "C:/Users/62856/Desktop/SHOPI/csv/bs_gabung.csv";
			}
			spark.read().format("CSV").option("inferSchema", "true").option("header", "true").load(path).createTempView(jsonObject.get("alias").getAsString());
		}
		
		Dataset<Row> result = spark.sql(query);
		result = result.limit(300);
		String[] column = result.columns();
		result = result.withColumn("No", functions.row_number().over(Window.orderBy(column[0].toString())));
		
		Long total = result.count();
		
		String condition = "No >" + start;
		result = result.where(condition);
		result = result.limit(length);

		List<String> paginatedResults = result.toJSON().collectAsList();
		
		spark.stop();
		
		return "{" + 
				"\"draw\":" + draw + 
				",\"recordsTotal\":" + total +
				",\"recordsFiltered\":" + total +
				",\"data\":" + paginatedResults +
				"}";
	};
	
	public static Route headerDataset2 = (Request request, Response response) -> {
		response.type("application/json");
		response.header("Access-Control-Allow-Origin", "*");
		
		String params = request.body();
		
		JsonParser parser = new JsonParser();
		JsonElement paramsJson = parser.parse(params);
		
		JsonObject paramsObject = paramsJson.getAsJsonObject();
		String query = paramsObject.get("query").getAsString();
		JsonArray df = paramsObject.get("dataframe").getAsJsonArray();
		
		var spark = SparkSession.builder().config(ApacheSparkHelper.getConfiguration()).getOrCreate();
		
		String path = "";
		for(int i = 0; i<df.size(); i++) {
			JsonObject jsonObject = df.get(i).getAsJsonObject();
			String name = jsonObject.get("dataset").getAsString();
			
			if(name.equals("rekap2018")) {
				path = "C:/Users/62856/Desktop/SHOPI/csv/rekap2018.csv";
			} 
			else if(name.equals("masterBlokSensus")) {
				path = "C:/Users/62856/Desktop/SHOPI/csv/mstbs_20121_st2013_ok.csv";
			} 
			else if(name.equals("hierarkiBlokSensus")) {
				path = "C:/Users/62856/Desktop/SHOPI/csv/hirarki_match_flag.csv";
			}  
			else if(name.equals("blokSensusGabung")) {
				path = "C:/Users/62856/Desktop/SHOPI/csv/bs_gabung.csv";
			}
			spark.read().format("CSV").option("inferSchema", "true").option("header", "true").load(path).createTempView(jsonObject.get("alias").getAsString());
		}
		
		Dataset<Row> result = spark.sql(query);
		
		String[] column = result.columns();
		
		String columns = "{\"data\":\"" + "No" + "\",\"defaultContent\":" + "\"\"," + "\"name\":\"" + "No" + "\"}";
		
		for (int i = 0; i<column.length; i++){
			columns = columns + ",{\"data\":\"" + column[i] + "\",\"defaultContent\":" + "\"\"," + "\"name\":\"" + column[i] + "\"}";
		}
		
		spark.stop();
		
		return "{\"columns\":[" + columns + "]}";
	};
	
	public static Route getFrsDatasetHeader = (Request request, Response response) -> {
		response.type("application/json");
		response.header("Access-Control-Allow-Origin", "*");
		
		String params = request.body();
		
		JsonParser parser = new JsonParser();
		JsonElement paramsJson = parser.parse(params);
		
		JsonObject paramsObject = paramsJson.getAsJsonObject();
		String query = paramsObject.get("query").getAsString();
		JsonArray df = paramsObject.get("dataframe").getAsJsonArray();
		
		var spark = SparkSession.builder().config(ApacheSparkHelper.getConfiguration()).getOrCreate();
		
		String path = "";
		for(int i = 0; i<df.size(); i++) {
			JsonObject jsonObject = df.get(i).getAsJsonObject();
			String programName = jsonObject.get("programName").getAsString();
			String cycleName = jsonObject.get("cycleName").getAsString();
			String versionName = jsonObject.get("versionName").getAsString();
			String datasetName = jsonObject.get("dataset").getAsString();
			Dataset<Row> dataset = getSparkDataset(programName, cycleName, versionName, datasetName);
			dataset.createTempView(jsonObject.get("alias").getAsString());
		}
		
		Dataset<Row> result = spark.sql(query);
		
		String[] column = result.columns();
		
		String columns = "{\"data\":\"" + "No" + "\",\"defaultContent\":" + "\"\"," + "\"name\":\"" + "No" + "\"}";
		
		for (int i = 0; i<column.length; i++){
			columns = columns + ",{\"data\":\"" + column[i] + "\",\"defaultContent\":" + "\"\"," + "\"name\":\"" + column[i] + "\"}";
		}
				
		return "{\"columns\":[" + columns + "]}";
	};
	
	public static Route getFrsDataset = (Request request, Response response) -> {
		response.type("application/json");
		response.header("Access-Control-Allow-Origin", "*");
		
		String params = request.body();
		
		JsonParser parser = new JsonParser();
		JsonElement paramsJson = parser.parse(params);
		
		JsonObject paramsObject = paramsJson.getAsJsonObject();
		Integer draw = paramsObject.get("draw").getAsInt();
		Integer length = paramsObject.get("length").getAsInt();
		Integer start = paramsObject.get("start").getAsInt();
		JsonArray df = paramsObject.get("dataframe").getAsJsonArray();
		String query = paramsObject.get("query").getAsString();
		
		var spark = SparkSession.builder().config(ApacheSparkHelper.getConfiguration()).getOrCreate();
		
		String path = "";
		for(int i = 0; i<df.size(); i++) {
			JsonObject jsonObject = df.get(i).getAsJsonObject();
			String programName = jsonObject.get("programName").getAsString();
			String cycleName = jsonObject.get("cycleName").getAsString();
			String versionName = jsonObject.get("versionName").getAsString();
			String datasetName = jsonObject.get("dataset").getAsString();
			Dataset<Row> dataset = getSparkDataset(programName, cycleName, versionName, datasetName);
			dataset.createTempView(jsonObject.get("alias").getAsString());
		}
		
		Dataset<Row> result = spark.sql(query);
		result = result.limit(300);
		String[] column = result.columns();
		result = result.withColumn("No", functions.row_number().over(Window.orderBy(column[0].toString())));
		
		Long total = result.count();
		
		String condition = "No >" + start;
		result = result.where(condition);
		result = result.limit(length);

		List<String> paginatedResults = result.toJSON().collectAsList();
		
		spark.stop();
		
		return "{" + 
				"\"draw\":" + draw + 
				",\"recordsTotal\":" + total +
				",\"recordsFiltered\":" + total +
				",\"data\":" + paginatedResults +
				"}";
	};
	
	private static Dataset<Row> getSparkDataset(String programName, String cycleName, String versionName, String datasetName)
	{
		return null;
	}
	
	public static Route getDataset = (Request request, Response response) -> {
		response.type("application/json");
		response.header("Access-Control-Allow-Origin", "*");
		
		String params = request.body();
		
		JsonParser parser = new JsonParser();
		JsonElement paramsJson = parser.parse(params);
		
		JsonObject paramsObject = paramsJson.getAsJsonObject();
		Integer draw = paramsObject.get("draw").getAsInt();
		Integer length = paramsObject.get("length").getAsInt();
		Integer start = paramsObject.get("start").getAsInt();
		String dfName = paramsObject.get("dfName").getAsString();
		
		String path = "";
		
		if(dfName.equals("rekap2018")) {
			path = "C:/Users/62856/Desktop/SHOPI/csv/rekap2018.csv";
		} 
		else if(dfName.equals("masterBlokSensus")) {
			path = "C:/Users/62856/Desktop/SHOPI/csv/mstbs_20121_st2013_ok.csv";
		} 
		else if(dfName.equals("hierarkiBlokSensus")) {
			path = "C:/Users/62856/Desktop/SHOPI/csv/hirarki_match_flag.csv";
		}  
		else if(dfName.equals("blokSensusGabung")) {
			path = "C:/Users/62856/Desktop/SHOPI/csv/bs_gabung.csv";
		}
		
		var spark = SparkSession.builder().config(ApacheSparkHelper.getConfiguration()).getOrCreate();
		Dataset<Row> dataset = spark.read().format("csv").option("header", "true").load(path);
		dataset.createOrReplaceTempView("df");
		
		Dataset<Row> result = spark.sql("SELECT * FROM df");
		String[] column = result.columns();
		result = result.withColumn("No", functions.row_number().over(Window.orderBy(column[0].toString())));
		
		Long total = result.count();
		
		String condition = "No >" + start;
		result = result.where(condition);
		result = result.limit(length);

		List<String> paginatedResults = result.toJSON().collectAsList();
		
		spark.stop();
		
		return "{" + 
				"\"draw\":" + draw + 
				",\"recordsTotal\":" + total +
				",\"recordsFiltered\":" + total +
				",\"data\":" + paginatedResults +
				"}";
	};
	
	public static Route headerDataset = (Request request, Response response) -> {
		response.type("application/json");
		response.header("Access-Control-Allow-Origin", "*");
		
		String params = request.body();
		
		JsonParser parser = new JsonParser();
		JsonElement paramsJson = parser.parse(params);
		
		JsonObject paramsObject = paramsJson.getAsJsonObject();
		String dfName = paramsObject.get("dfName").getAsString();
		
		String path = "";
		
		if(dfName.equals("rekap2018")) {
			path = "C:/Users/62856/Desktop/SHOPI/csv/rekap2018.csv";
		} 
		else if(dfName.equals("masterBlokSensus")) {
			path = "C:/Users/62856/Desktop/SHOPI/csv/mstbs_20121_st2013_ok.csv";
		} 
		else if(dfName.equals("hierarkiBlokSensus")) {
			path = "C:/Users/62856/Desktop/SHOPI/csv/hirarki_match_flag.csv";
		}  
		else if(dfName.equals("blokSensusGabung")) {
			path = "C:/Users/62856/Desktop/SHOPI/csv/bs_gabung.csv";
		}
		
		var spark = SparkSession.builder().config(ApacheSparkHelper.getConfiguration()).getOrCreate();
		
		Dataset<Row> df = spark.read().format("CSV").option("inferSchema", "true").option("header", "true").load(path);
		
		String[] column = df.columns();
		
		String columns = "{\"data\":\"" + "No" + "\",\"defaultContent\":" + "\"\"," + "\"name\":\"" + "No" + "\"}";
		
		for (int i = 0; i<column.length; i++){
			columns = columns + ",{\"data\":\"" + column[i] + "\",\"defaultContent\":" + "\"\"," + "\"name\":\"" + column[i] + "\"}";
		}
		
		spark.stop();
		
		return "{\"columns\":[" + columns + "]}";
	};
	
	public static Route joinDataset3 = (Request request, Response response) -> {
		response.type("application/json");
		response.header("Access-Control-Allow-Origin", "*");
		
		String params = request.body();
		
		JsonParser parser = new JsonParser();
		JsonElement paramsJson = parser.parse(params);
		
		JsonObject paramsObject = paramsJson.getAsJsonObject();
		Integer draw = paramsObject.get("draw").getAsInt();
		Integer length = paramsObject.get("length").getAsInt();
		Integer start = paramsObject.get("start").getAsInt();
		String query = paramsObject.get("query").getAsString();
		
		var spark = SparkSession.builder().config(ApacheSparkHelper.getConfiguration()).getOrCreate();
		
		Dataset<Row> result = spark.sql(query);
		String [] column = result.columns();
		result = result.withColumn("No", functions.row_number().over(Window.orderBy(column[0].toString())));
		
		Long total = result.count();
		
		String condition = "No >" + start;
		result = result.where(condition);
		result = result.limit(length);

		List<String> paginatedResults = result.toJSON().collectAsList();
		
		spark.stop();
		
		return "{" + 
				"\"draw\":" + draw + 
				",\"recordsTotal\":" + total +
				",\"recordsFiltered\":" + total +
				",\"data\":" + paginatedResults +
				"}";
	};
	
	
	public static Route joinDataset2 = (Request request, Response response) -> {
		response.type("application/json");
		response.header("Access-Control-Allow-Origin", "*");
		
		String params = request.body();
		
		JsonParser parser = new JsonParser();
		JsonElement paramsJson = parser.parse(params);
		
		JsonObject paramsObject = paramsJson.getAsJsonObject();
		Integer draw = paramsObject.get("draw").getAsInt();
		Integer length = paramsObject.get("length").getAsInt();
		Integer start = paramsObject.get("start").getAsInt();
		JsonArray df = paramsObject.get("dataframe").getAsJsonArray();
		
		var spark = SparkSession.builder().config(ApacheSparkHelper.getConfiguration()).getOrCreate();
		
		String path = "";
		for(int i = 0; i<df.size(); i++) {
			JsonObject jsonObject = df.get(i).getAsJsonObject();
			String name = jsonObject.get("name").getAsString();
			
			if(name.equals("rekap2018")) {
				path = "C:/Users/62856/Desktop/SHOPI/csv/rekap2018.csv";
			} 
			else if(name.equals("masterBlokSensus")) {
				path = "C:/Users/62856/Desktop/SHOPI/csv/mstbs_20121_st2013_ok.csv";
			} 
			else if(name.equals("hierarkiBlokSensus")) {
				path = "C:/Users/62856/Desktop/SHOPI/csv/hirarki_match_flag.csv";
			}  
			else if(name.equals("blokSensusGabung")) {
				path = "C:/Users/62856/Desktop/SHOPI/csv/bs_gabung.csv";
			}
			spark.read().format("CSV").option("inferSchema", "true").option("header", "true").load(path).createTempView(jsonObject.get("alias").getAsString());
		}
		
		Dataset<Row> result = spark.sql("SELECT df1.* FROM df1 JOIN df2 ON df1.bs12_0 = df2.bs12_0 LIMIT 1000");
		String [] column = result.columns();
		result = result.withColumn("No", functions.row_number().over(Window.orderBy(column[0].toString())));
		
		Long total = result.count();
		
		String condition = "No >" + start;
		result = result.where(condition);
		result = result.limit(length);

		List<String> paginatedResults = result.toJSON().collectAsList();
		
		spark.stop();
		
		return "{" + 
				"\"draw\":" + draw + 
				",\"recordsTotal\":" + total +
				",\"recordsFiltered\":" + total +
				",\"data\":" + paginatedResults +
				"}";
	};
	
	public static Route joinDataset = (Request request, Response response) -> {
		response.type("application/json");
		response.header("Access-Control-Allow-Origin", "*");
		
		var spark = SparkSession.builder().config(ApacheSparkHelper.getConfiguration()).getOrCreate();
		
		String params = request.body();
		
		JsonParser parser = new JsonParser();
		JsonElement paramsJson = parser.parse(params);
		
		JsonObject paramsObject = paramsJson.getAsJsonObject();
		JsonArray df = paramsObject.get("dataframe").getAsJsonArray();
		Integer draw = paramsObject.get("draw").getAsInt();
		Integer length = paramsObject.get("length").getAsInt();
		Integer start = paramsObject.get("start").getAsInt();
		
		String path = "";
		for(int i = 0; i<df.size(); i++) {
			JsonObject jsonObject = df.get(i).getAsJsonObject();
			String name = jsonObject.get("name").getAsString();
			//String alias = jsonObject.get("alias").getAsString();
			
			if(name.equals("rekap2018")) {
				path = "C:/Users/62856/Desktop/SHOPI/csv/rekap2018.csv";
			} 
			else if(name.equals("masterBlokSensus")) {
				path = "C:/Users/62856/Desktop/SHOPI/csv/mstbs_20121_st2013_ok.csv";
			} 
			else if(name.equals("hierarkiBlokSensus")) {
				path = "C:/Users/62856/Desktop/SHOPI/csv/hirarki_match_flag.csv";
			}  
			else if(name.equals("blokSensusGabung")) {
				path = "C:/Users/62856/Desktop/SHOPI/csv/bs_gabung.csv";
			}
			spark.read().format("CSV").option("inferSchema", "true").option("header", "true").load(path).createTempView(jsonObject.get("alias").getAsString());
		}
		
		Dataset<Row> result = spark.sql("SELECT b.* FROM masterBlokSensus a FULL OUTER JOIN blokSensusGabung b ON a.desa = b.desa");
		String [] column = result.columns();
		result = result.withColumn("No", functions.row_number().over(Window.orderBy("nbsc1")));
		
		/*
		Long total = result.count();
		
		String condition = "No >" + start;
		result = result.where(condition);
		result = result.limit(length);
		
		String columns = "";
		
		for (int i = 0; i<column.length; i++){
			if(i == 0) {
				columns = columns + "{\"data\":\"" + column[i] + "\",\"defaultContent\":" + "\"\"," + "\"name\":\"" + column[i] + "\"}";
			} else {
				columns = columns + ",{\"data\":\"" + column[i] + "\",\"defaultContent\":" + "\"\"," + "\"name\":\"" + column[i] + "\"}";
			}
		}
		
		columns = "[" + columns + "]";

		List<String> paginatedResults = result.toJSON().collectAsList();
		*/
		spark.stop();
		
		return "test";
	};
	
	public static Route summarize = (Request request, Response response) -> {
		response.type("application/json");
		response.header("Access-Control-Allow-Origin", "*");
	
		
		String query = "SELECT * FROM ";
		
		return "Test";
	};
	
	public static Route summarize2 = (Request request, Response response) -> {
		response.type("application/json");
		response.header("Access-Control-Allow-Origin", "*");
		var spark = SparkSession.builder().config(ApacheSparkHelper.getConfiguration()).getOrCreate();
		
		var path1 = "C:/Users/62856/Desktop/SHOPI/csv/rekap2018.csv";
		Dataset<Row> df = spark.read().format("CSV").option("inferSchema", "true").option("header", "true").load(path1);
		String dfname = "df";
		df.createTempView(dfname);
		
		String params = request.body();
		
		JsonParser parser = new JsonParser();
		JsonElement jsonTree = parser.parse(params);
		
		JsonObject jsonObject = jsonTree.getAsJsonObject();
		JsonArray method = jsonObject.getAsJsonArray("method");
		JsonArray field = jsonObject.getAsJsonArray("field");
		
		String query = "SELECT *";
		
		for(int i=0; i<method.size(); i++) {
			String metode = method.get(i).getAsString();
			if(metode.equals("SUM")) {
				query = query + ",(";
				for(int j=0; j<field.size(); j++) {
					if(j == 0) {
						String column = field.get(j).getAsString();
						column = "`" + column + "`";
						query = query + column;
					} else {
						query = query + "+";
						String column = field.get(j).getAsString();
						column = "`" + column + "`";
						query = query + column;
					}
				}
				query = query + ") AS Sum ";
			}
			else if(metode.equals("MEAN")) {
				query = query + ",((";
				for(int j=0; j<field.size(); j++) {
					if(j == 0) {
						String column = field.get(j).getAsString();
						column = "`" + column + "`";
						query = query + column;
					} else {
						query = query + "+";
						String column = field.get(j).getAsString();
						column = "`" + column + "`";
						query = query + column;
					}
				}
				query = query + ")/" + field.size() + ") AS Avg ";
			}
			else if(metode.equals("MIN")) {
				query = query + ",(LEAST(";
				for(int j=0; j<field.size(); j++) {
					if(j == 0) {
						String column = field.get(j).getAsString();
						column = "`" + column + "`";
						query = query + column;
					} else {
						query = query + ",";
						String column = field.get(j).getAsString();
						column = "`" + column + "`";
						query = query + column;
					}
				}
				query = query + ")) AS Min ";
			}
			else if(metode.equals("MAX")) {
				query = query + ",(LEAST(";
				for(int j=0; j<field.size(); j++) {
					if(j == 0) {
						String column = field.get(j).getAsString();
						column = "`" + column + "`";
						query = query + column;
					} else {
						query = query + ",";
						String column = field.get(j).getAsString();
						column = "`" + column + "`";
						query = query + column;
					}
				}
				query = query + ")) AS Max ";
			}
			else if(metode.equals("COUNT")) {
				query = query + ",(" + field.size() + ") AS COUNT ";
			}
		}
		
		query = query + "FROM " + dfname;
		
		var paths = "C:/Users/62856/Desktop/SHOPI/csv/rekap2018.csv";
		Dataset<Row> dfs = spark.read().format("CSV").option("inferSchema", "true").option("header", "true").load(paths);
		dfs.createTempView("dfs");
		
		Dataset<Row> result = spark.sql(query);
		
		List<String> paginatedResults = result.toJSON().collectAsList();
		
		spark.stop();
		
		return "{" + 
					"\"data\":" + paginatedResults +
				"}";
		
		/*
		spark.udf().register("kum_sized", new UDF2<Integer, Integer, Integer>() {
			@Override
			public Integer call(Integer kum_size, Integer kd_str_awl) throws Exception {
				// TODO Auto-generated method stub
				Integer a = 0;
				if(kd_str_awl == 211) {
					a =1;
				} else {
					a = 2;
				}
				return a;
			}
		}, DataTypes.IntegerType);
		
		rekap_stra = rekap_stra.withColumn("kum_size", functions.lit(0));
		rekap_stra.createTempView("kum");
		
		rekap_stra = spark.sql("SELECT *, kum_sized(kum_size,kd_str_awl) AS kum_sized FROM kum");
		rekap_stra.show();
		*/
	};
	
	public static Route explorasi = (Request request, Response response) -> {
		response.type("application/json");
		response.header("Access-Control-Allow-Origin", "*");
		
		String params = request.body();
		
		JsonParser parser = new JsonParser();
		JsonElement paramsJson = parser.parse(params);
		
		JsonObject paramsObject = paramsJson.getAsJsonObject();
		Boolean headers = paramsObject.get("headers").getAsBoolean();
		JsonArray dataset = paramsObject.get("dataframe").getAsJsonArray();
		String query = paramsObject.get("query").getAsString();
		
		var spark = SparkSession.builder().config(ApacheSparkHelper.getConfiguration()).getOrCreate();
		String path = "";
		String result = null;
		for(int i = 0; i<dataset.size(); i++) {
			JsonObject jsonObject = dataset.get(i).getAsJsonObject();
			String name = jsonObject.get("dataset").getAsString();
			
			if(name.equals("rekap2018")) {
				path = "C:/Users/62856/Desktop/SHOPI/csv/rekap2018.csv";
			} 
			else if(name.equals("masterBlokSensus")) {
				path = "C:/Users/62856/Desktop/SHOPI/csv/mstbs_20121_st2013_ok.csv";
			} 
			else if(name.equals("hierarkiBlokSensus")) {
				path = "C:/Users/62856/Desktop/SHOPI/csv/hirarki_match_flag.csv";
			}  
			else if(name.equals("blokSensusGabung")) {
				path = "C:/Users/62856/Desktop/SHOPI/csv/bs_gabung.csv";
			}
			spark.read().format("CSV").option("inferSchema", "true").option("header", "true").load(path).createTempView(jsonObject.get("alias").getAsString());
		}
		
		Dataset<Row> df = spark.sql(query);
		df = df.limit(300);
		
		if(headers.equals(true)) {
			var header = df.columns();
			
			String columns = "{\"data\":\"" + "No" + "\",\"defaultContent\":" + "\"\"," + "\"name\":\"" + "No" + "\"}";
			
			for (int i = 0; i<header.length; i++){
				columns = columns + ",{\"data\":\"" + header[i] + "\",\"defaultContent\":" + "\"\"," + "\"name\":\"" + header[i] + "\"}";
			}
			
			result = "{\"columns\":[" + columns + "]}";
			
			spark.stop();
		} else {
			Integer draw = paramsObject.get("draw").getAsInt();
			Integer length = paramsObject.get("length").getAsInt();
			Integer start = paramsObject.get("start").getAsInt();
			
			String[] column = df.columns();
			df = df.withColumn("No", functions.row_number().over(Window.orderBy(column[0].toString())));
			
			Long total = df.count();
			
			String condition = "No >" + start;
			df = df.where(condition);
			df = df.limit(length);

			List<String> paginatedResults = df.toJSON().collectAsList();
			
			spark.stop();
			
			result ="{" + 
						"\"draw\":" + draw + 
						",\"recordsTotal\":" + total +
						",\"recordsFiltered\":" + total +
						",\"data\":" + paginatedResults +
					"}";
		}
		
		return result;
	};
}
