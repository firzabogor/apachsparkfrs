package controller;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.OptionalInt;
import java.util.Properties;
import java.util.stream.Collectors;

import org.apache.spark.SparkConf;
import org.apache.spark.SparkContext;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.api.java.UDF1;
import org.apache.spark.sql.api.java.UDF2;
import org.apache.spark.sql.api.java.UDF3;
import org.apache.spark.sql.api.java.UDF4;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructType;
import org.eclipse.jetty.websocket.common.frames.DataFrame;
import org.apache.spark.sql.functions;
import org.apache.spark.sql.expressions.Window;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import helpers.ApacheSparkHelper;
import helpers.ServiceHelper;
import io.netty.handler.codec.http.HttpContentEncoder.Result;
import lombok.var;
import models.Minimum;
import scala.annotation.meta.field;
import scala.collection.Seq;
import scala.collection.immutable.Stream;
import scala.collection.mutable.WrappedArray;
import services.FrsService;
import spark.Request;
import spark.Response;
import spark.Route;

public class DataController {
	
	public static Route live = (Request request, Response response) -> {
		response.type("application/json");
		response.header("Access-Control-Allow-Origin", "*");
		
		return "live";
	};
	
	public static Route explorasiSave = (Request request, Response response) -> {
		response.type("application/json");
		response.header("Access-Control-Allow-Origin", "*");
		
		String params = request.body();
		
		JsonParser parser = new JsonParser();
		JsonElement paramsJson = parser.parse(params);
		
		JsonObject paramsObject = paramsJson.getAsJsonObject();
		JsonArray dataframe = paramsObject.get("dataframe").getAsJsonArray();
		JsonArray explorasi = paramsObject.get("explorasi").getAsJsonArray();
		String result = null;
		
		var conf = ApacheSparkHelper.getConfiguration();
		var spark = SparkSession.builder().config(conf).getOrCreate();
		
		try {
			var url = "jdbc:sqlserver://localhost:1433";
			var table = "";
			
			var properties = new Properties();
			properties.put("user","sa");
			properties.put("password","Blackrose9");
			
			String path = "";
			Dataset<Row> df = null;
			
			for(int i = 0; i<dataframe.size(); i++) {
				JsonObject jsonObject = dataframe.get(i).getAsJsonObject();
				String dname = jsonObject.get("dataset").getAsString();
				
				if(dname.equals("rekap2018")) {
					path = "C:/Users/62856/Desktop/SHOPI/csv/rekap2018.csv";
					spark.read().format("CSV").option("inferSchema", "true").option("header", "true").load(path).createTempView(jsonObject.get("alias").getAsString());
				} 
				else if(dname.equals("masterBlokSensus")) {
					path = "C:/Users/62856/Desktop/SHOPI/csv/mstbs_20121_st2013_ok.csv";
					spark.read().format("CSV").option("inferSchema", "true").option("header", "true").load(path).createTempView(jsonObject.get("alias").getAsString());
				} 
				else if(dname.equals("hierarkiBlokSensus")) {
					path = "C:/Users/62856/Desktop/SHOPI/csv/hirarki_match_flag.csv";
					spark.read().format("CSV").option("inferSchema", "true").option("header", "true").load(path).createTempView(jsonObject.get("alias").getAsString());
				}  
				else if(dname.equals("blokSensusGabung")) {
					path = "C:/Users/62856/Desktop/SHOPI/csv/bs_gabung.csv";
					spark.read().format("CSV").option("inferSchema", "true").option("header", "true").load(path).createTempView(jsonObject.get("alias").getAsString());
				}
				else if(dname.equals("bisnisPerusahaan")) {
					table = "dbo.business_perusahaan";
					df = spark.read().jdbc(url, table, properties);
					df.createTempView(jsonObject.get("alias").getAsString());
				}
				else if(dname.equals("bisnisEmail")) {
					table = "dbo.business_alamat_email";
					df = spark.read().jdbc(url, table, properties);
					df.createTempView(jsonObject.get("alias").getAsString());
				}
				else if(dname.equals("bisnisFax")) {
					table = "dbo.business_alamat_fax";
					df = spark.read().jdbc(url, table, properties);
					df.createTempView(jsonObject.get("alias").getAsString());
				}
				else if(dname.equals("bisnisTelepon")) {
					table = "dbo.business_alamat_telepon";
					df = spark.read().jdbc(url, table, properties);
					df.createTempView(jsonObject.get("alias").getAsString());
				}
				else if(dname.equals("bisnisWeb")) {
					table = "dbo.business_alamat_web";
					df = spark.read().jdbc(url, table, properties);
					df.createTempView(jsonObject.get("alias").getAsString());
				}
				else if(dname.equals("bisnisAktivitasPerusahaan")) {
					table = "dbo.business_aktivitas_perushaan";
					df = spark.read().jdbc(url, table, properties);
					df.createTempView(jsonObject.get("alias").getAsString());
				}
				else if(dname.equals("bisnisKontakPerusahaan")) {
					table = "dbo.dbo.business_kontak_perusahaan";
					df = spark.read().jdbc(url, table, properties);
					df.createTempView(jsonObject.get("alias").getAsString());
				}
				else if(dname.equals("bisnisPemegangSaham")) {
					table = "dbo.business_pemegang_saham";
					df = spark.read().jdbc(url, table, properties);
					df.createTempView(jsonObject.get("alias").getAsString());
				}
				else if(dname.equals("bisnisProdukPerusahaan")) {
					table = "dbo.business_produk_perusahaan";
					df = spark.read().jdbc(url, table, properties);
					df.createTempView(jsonObject.get("alias").getAsString());
				}
				else if(dname.equals("peoplePenduduk")) {
					table = "dbo.people_penduduk";
					df = spark.read().jdbc(url, table, properties);
					df.createTempView(jsonObject.get("alias").getAsString());
				}
				else if(dname.equals("peoplePendudukDefacto")) {
					table = "dbo.people_penduduk_defacto";
					df = spark.read().jdbc(url, table, properties);
					df.createTempView(jsonObject.get("alias").getAsString());
				}
				else if(dname.equals("familyKeluarga")) {
					table = "dbo.family_keluarga";
					df = spark.read().jdbc(url, table, properties);
					df.createTempView(jsonObject.get("alias").getAsString());
				}
				else if(dname.equals("familyKeluargaDefacto")) {
					table = "dbo.family_keluarga_defacto";
					df = spark.read().jdbc(url, table, properties);
					df.createTempView(jsonObject.get("alias").getAsString());
				}
				else if(dname.equals("areaSnapshotWilayah")) {
					table = "dbo.area_snapshot_wilayah";
					df = spark.read().jdbc(url, table, properties);
					df.createTempView(jsonObject.get("alias").getAsString());
				}
				else if(dname.equals("areaProvinsi")) {
					table = "dbo.area_provinsi";
					df = spark.read().jdbc(url, table, properties);
					df.createTempView(jsonObject.get("alias").getAsString());
				}
				else if(dname.equals("areaKabupatenKota")) {
					table = "dbo.area_kabupaten_kota";
					df = spark.read().jdbc(url, table, properties);
					df.createTempView(jsonObject.get("alias").getAsString());
				}
				else if(dname.equals("areaKecamatan")) {
					table = "dbo.area_kecamatan";
					df = spark.read().jdbc(url, table, properties);
					df.createTempView(jsonObject.get("alias").getAsString());
				}
				else if(dname.equals("areaKeluarahanDesa")) {
					table = "dbo.area_kelurahan_desa";
					df = spark.read().jdbc(url, table, properties);
					df.createTempView(jsonObject.get("alias").getAsString());
				}
				else if(dname.equals("areaBlokSensus")) {
					table = "dbo.area_blok_sensus";
					df = spark.read().jdbc(url, table, properties);
					df.createTempView(jsonObject.get("alias").getAsString());
				}
				else if(dname.equals("areaSatuanLingkunganSetempat")) {
					table = "dbo.area_satuan_lingkungan_setempat";
					df = spark.read().jdbc(url, table, properties);
					df.createTempView(jsonObject.get("alias").getAsString());
				}
			}
			
			for(int j = 0; j<explorasi.size(); j++) {
				JsonObject explorasiObject = explorasi.get(j).getAsJsonObject();
				String query = explorasiObject.get("query").getAsString();
				String alias2 = explorasiObject.get("alias2").getAsString();
				df = spark.sql(query);
				df.createOrReplaceTempView(alias2);
			}
			
			df.createOrReplaceTempView("df");
			
			Dataset<Row> results = spark.sql("SELECT * FROM df");

			List<String> paginatedResults = results.toJSON().collectAsList();
			
			result = "{" + 
						"\"data\":" + paginatedResults +
						",\"status\":\"succeed\""+
					"}";
			
			spark.stop();
		} catch(Exception e) {
			result = "{\"status\":\"failed\"}";
		}
		spark.stop();
		return result;
	};
	
	
	
	public static Route datagui = (Request request, Response response) -> {
		response.type("application/json");
		response.header("Access-Control-Allow-Origin", "*");
		
		String params = request.body();
		
		JsonParser parser = new JsonParser();
		JsonElement paramsJson = parser.parse(params);
		
		JsonObject paramsObject = paramsJson.getAsJsonObject();
		Boolean headers = paramsObject.get("headers").getAsBoolean();
		//String dataset = paramsObject.get("dataset").getAsString();
		JsonArray dataframe = paramsObject.get("dataframe").getAsJsonArray();
		JsonArray explorasi = paramsObject.get("explorasi").getAsJsonArray();
		String result = null;
		
		var conf = ApacheSparkHelper.getConfiguration();
		var spark = SparkSession.builder().config(conf).getOrCreate();
		
		try {
			var url = "jdbc:sqlserver://localhost:1433";
			var table = "";
			
			var properties = new Properties();
			properties.put("user","sa");
			properties.put("password","Blackrose9");
			
			String path = "";
			Dataset<Row> df = null;
			
			for(int i = 0; i<dataframe.size(); i++) {
				JsonObject jsonObject = dataframe.get(i).getAsJsonObject();
				String dname = jsonObject.get("dataset").getAsString();
				
				if(dname.equals("rekap2018")) {
					path = "C:/Users/62856/Desktop/SHOPI/csv/rekap2018.csv";
					spark.read().format("CSV").option("inferSchema", "true").option("header", "true").load(path).createTempView(jsonObject.get("alias").getAsString());
				} 
				else if(dname.equals("masterBlokSensus")) {
					path = "C:/Users/62856/Desktop/SHOPI/csv/mstbs_20121_st2013_ok.csv";
					spark.read().format("CSV").option("inferSchema", "true").option("header", "true").load(path).createTempView(jsonObject.get("alias").getAsString());
				} 
				else if(dname.equals("hierarkiBlokSensus")) {
					path = "C:/Users/62856/Desktop/SHOPI/csv/hirarki_match_flag.csv";
					spark.read().format("CSV").option("inferSchema", "true").option("header", "true").load(path).createTempView(jsonObject.get("alias").getAsString());
				}  
				else if(dname.equals("blokSensusGabung")) {
					path = "C:/Users/62856/Desktop/SHOPI/csv/bs_gabung.csv";
					spark.read().format("CSV").option("inferSchema", "true").option("header", "true").load(path).createTempView(jsonObject.get("alias").getAsString());
				}
				else if(dname.equals("bisnisPerusahaan")) {
					table = "dbo.business_perusahaan";
					df = spark.read().jdbc(url, table, properties);
					df.createTempView(jsonObject.get("alias").getAsString());
				}
				else if(dname.equals("bisnisEmail")) {
					table = "dbo.business_alamat_email";
					df = spark.read().jdbc(url, table, properties);
					df.createTempView(jsonObject.get("alias").getAsString());
				}
				else if(dname.equals("bisnisFax")) {
					table = "dbo.business_alamat_fax";
					df = spark.read().jdbc(url, table, properties);
					df.createTempView(jsonObject.get("alias").getAsString());
				}
				else if(dname.equals("bisnisTelepon")) {
					table = "dbo.business_alamat_telepon";
					df = spark.read().jdbc(url, table, properties);
					df.createTempView(jsonObject.get("alias").getAsString());
				}
				else if(dname.equals("bisnisWeb")) {
					table = "dbo.business_alamat_web";
					df = spark.read().jdbc(url, table, properties);
					df.createTempView(jsonObject.get("alias").getAsString());
				}
				else if(dname.equals("bisnisAktivitasPerusahaan")) {
					table = "dbo.business_aktivitas_perushaan";
					df = spark.read().jdbc(url, table, properties);
					df.createTempView(jsonObject.get("alias").getAsString());
				}
				else if(dname.equals("bisnisKontakPerusahaan")) {
					table = "dbo.dbo.business_kontak_perusahaan";
					df = spark.read().jdbc(url, table, properties);
					df.createTempView(jsonObject.get("alias").getAsString());
				}
				else if(dname.equals("bisnisPemegangSaham")) {
					table = "dbo.business_pemegang_saham";
					df = spark.read().jdbc(url, table, properties);
					df.createTempView(jsonObject.get("alias").getAsString());
				}
				else if(dname.equals("bisnisProdukPerusahaan")) {
					table = "dbo.business_produk_perusahaan";
					df = spark.read().jdbc(url, table, properties);
					df.createTempView(jsonObject.get("alias").getAsString());
				}
				else if(dname.equals("peoplePenduduk")) {
					table = "dbo.people_penduduk";
					df = spark.read().jdbc(url, table, properties);
					df.createTempView(jsonObject.get("alias").getAsString());
				}
				else if(dname.equals("peoplePendudukDefacto")) {
					table = "dbo.people_penduduk_defacto";
					df = spark.read().jdbc(url, table, properties);
					df.createTempView(jsonObject.get("alias").getAsString());
				}
				else if(dname.equals("familyKeluarga")) {
					table = "dbo.family_keluarga";
					df = spark.read().jdbc(url, table, properties);
					df.createTempView(jsonObject.get("alias").getAsString());
				}
				else if(dname.equals("familyKeluargaDefacto")) {
					table = "dbo.family_keluarga_defacto";
					df = spark.read().jdbc(url, table, properties);
					df.createTempView(jsonObject.get("alias").getAsString());
				}
				else if(dname.equals("areaSnapshotWilayah")) {
					table = "dbo.area_snapshot_wilayah";
					df = spark.read().jdbc(url, table, properties);
					df.createTempView(jsonObject.get("alias").getAsString());
				}
				else if(dname.equals("areaProvinsi")) {
					table = "dbo.area_provinsi";
					df = spark.read().jdbc(url, table, properties);
					df.createTempView(jsonObject.get("alias").getAsString());
				}
				else if(dname.equals("areaKabupatenKota")) {
					table = "dbo.area_kabupaten_kota";
					df = spark.read().jdbc(url, table, properties);
					df.createTempView(jsonObject.get("alias").getAsString());
				}
				else if(dname.equals("areaKecamatan")) {
					table = "dbo.area_kecamatan";
					df = spark.read().jdbc(url, table, properties);
					df.createTempView(jsonObject.get("alias").getAsString());
				}
				else if(dname.equals("areaKeluarahanDesa")) {
					table = "dbo.area_kelurahan_desa";
					df = spark.read().jdbc(url, table, properties);
					df.createTempView(jsonObject.get("alias").getAsString());
				}
				else if(dname.equals("areaBlokSensus")) {
					table = "dbo.area_blok_sensus";
					df = spark.read().jdbc(url, table, properties);
					df.createTempView(jsonObject.get("alias").getAsString());
				}
				else if(dname.equals("areaSatuanLingkunganSetempat")) {
					table = "dbo.area_satuan_lingkungan_setempat";
					df = spark.read().jdbc(url, table, properties);
					df.createTempView(jsonObject.get("alias").getAsString());
				}
			}
			
			for(int j = 0; j<explorasi.size(); j++) {
				JsonObject explorasiObject = explorasi.get(j).getAsJsonObject();
				String query = explorasiObject.get("query").getAsString();
				String alias2 = explorasiObject.get("alias2").getAsString();
				df = spark.sql(query);
				df.createOrReplaceTempView(alias2);
			}
			
			if(headers.equals(true)) {
				var header = df.columns();
				
				String columns = "{\"data\":\"" + "No" + "\",\"defaultContent\":" + "\"\"," + "\"name\":\"" + "No" + "\"}";
				
				for (int i = 0; i<header.length; i++){
					columns = columns + ",{\"data\":\"" + header[i] + "\",\"defaultContent\":" + "\"\"," + "\"name\":\"" + header[i] + "\"}";
				}
				
				result = "{\"columns\":[" + columns + "],\"status\":\"succeed\"}";
				
				spark.stop();
			} else {
				Integer draw = paramsObject.get("draw").getAsInt();
				Integer length = paramsObject.get("length").getAsInt();
				Integer start = paramsObject.get("start").getAsInt();

				df.createOrReplaceTempView("df");
				
				Dataset<Row> results = spark.sql("SELECT * FROM df");
				String[] column = results.columns();
				results = results.withColumn("No", functions.row_number().over(Window.orderBy(column[0].toString())));
				
				Long total = results.count();
				
				String condition = "No >" + start;
				results = results.where(condition);
				results = results.limit(length);

				List<String> paginatedResults = results.toJSON().collectAsList();
				
				result = "{" + 
							"\"draw\":" + draw + 
							",\"recordsTotal\":" + total +
							",\"recordsFiltered\":" + total +
							",\"data\":" + paginatedResults +
							",\"status\":\"succeed\""+
						"}";
				
				spark.stop();
			}
		} catch(Exception e) {
			result = "{\"status\":\"failed\"}";
		}
		spark.stop();
		return result;
	};
	
	public static Route dataset = (Request request, Response response) -> {
		response.type("application/json");
		response.header("Access-Control-Allow-Origin", "*");
		
		String params = request.body();
		
		JsonParser parser = new JsonParser();
		JsonElement paramsJson = parser.parse(params);
		
		JsonObject paramsObject = paramsJson.getAsJsonObject();
		Boolean headers = paramsObject.get("headers").getAsBoolean();
		String dataset = paramsObject.get("dataset").getAsString();
		String result = null;
		var conf = ApacheSparkHelper.getConfiguration();
		var spark = SparkSession.builder().config(conf).getOrCreate();
		
		try {
			var url = "jdbc:sqlserver://localhost:1433";
			String table = null;
			
			var properties = new Properties();
			properties.put("user","sa");
			properties.put("password","Blackrose9");
			
			String path = "";
			Dataset<Row> df = null;
			
			if(dataset.equals("rekap2018")) {
				path = "C:/Users/62856/Desktop/SHOPI/csv/rekap2018.csv";
				df = spark.read().format("CSV").option("inferSchema", "true").option("header", "true").load(path);
			} 
			else if(dataset.equals("masterBlokSensus")) {
				path = "C:/Users/62856/Desktop/SHOPI/csv/mstbs_20121_st2013_ok.csv";
				df = spark.read().format("CSV").option("inferSchema", "true").option("header", "true").load(path);
			} 
			else if(dataset.equals("hierarkiBlokSensus")) {
				path = "C:/Users/62856/Desktop/SHOPI/csv/hirarki_match_flag.csv";
				df = spark.read().format("CSV").option("inferSchema", "true").option("header", "true").load(path);
			}  
			else if(dataset.equals("blokSensusGabung")) {
				path = "C:/Users/62856/Desktop/SHOPI/csv/bs_gabung.csv";
				df = spark.read().format("CSV").option("inferSchema", "true").option("header", "true").load(path);
			}
			else if(dataset.equals("bisnisPerusahaan")) {
				table = "dbo.business_perusahaan";
				df = spark.read().jdbc(url, table, properties);
			}
			else if(dataset.equals("bisnisEmail")) {
				table = "dbo.business_alamat_email";
				df = spark.read().jdbc(url, table, properties);
			}
			else if(dataset.equals("bisnisFax")) {
				table = "dbo.business_alamat_fax";
				df = spark.read().jdbc(url, table, properties);
			}
			else if(dataset.equals("bisnisTelepon")) {
				table = "dbo.business_alamat_telepon";
				df = spark.read().jdbc(url, table, properties);
			}
			else if(dataset.equals("bisnisWeb")) {
				table = "dbo.business_alamat_web";
				df = spark.read().jdbc(url, table, properties);
			}
			else if(dataset.equals("bisnisAktivitasPerusahaan")) {
				table = "dbo.business_aktivitas_perushaan";
				df = spark.read().jdbc(url, table, properties);
			}
			else if(dataset.equals("bisnisKontakPerusahaan")) {
				table = "dbo.dbo.business_kontak_perusahaan";
				df = spark.read().jdbc(url, table, properties);
			}
			else if(dataset.equals("bisnisPemegangSaham")) {
				table = "dbo.business_pemegang_saham";
				df = spark.read().jdbc(url, table, properties);
			}
			else if(dataset.equals("bisnisProdukPerusahaan")) {
				table = "dbo.business_produk_perusahaan";
				df = spark.read().jdbc(url, table, properties);
			}
			else if(dataset.equals("peoplePenduduk")) {
				table = "dbo.people_penduduk";
				df = spark.read().jdbc(url, table, properties);
			}
			else if(dataset.equals("peoplePendudukDefacto")) {
				table = "dbo.people_penduduk_defacto";
				df = spark.read().jdbc(url, table, properties);
			}
			else if(dataset.equals("familyKeluarga")) {
				table = "dbo.family_keluarga";
				df = spark.read().jdbc(url, table, properties);
			}
			else if(dataset.equals("familyKeluargaDefacto")) {
				table = "dbo.family_keluarga_defacto";
				df = spark.read().jdbc(url, table, properties);
			}
			else if(dataset.equals("areaSnapshotWilayah")) {
				table = "dbo.area_snapshot_wilayah";
				df = spark.read().jdbc(url, table, properties);
			}
			else if(dataset.equals("areaProvinsi")) {
				table = "dbo.area_provinsi";
				df = spark.read().jdbc(url, table, properties);
			}
			else if(dataset.equals("areaKabupatenKota")) {
				table = "dbo.area_kabupaten_kota";
				df = spark.read().jdbc(url, table, properties);
			}
			else if(dataset.equals("areaKecamatan")) {
				table = "dbo.area_kecamatan";
				df = spark.read().jdbc(url, table, properties);
			}
			else if(dataset.equals("areaKeluarahanDesa")) {
				table = "dbo.area_kelurahan_desa";
				df = spark.read().jdbc(url, table, properties);
			}
			else if(dataset.equals("areaBlokSensus")) {
				table = "dbo.area_blok_sensus";
				df = spark.read().jdbc(url, table, properties);
			}
			else if(dataset.equals("areaSatuanLingkunganSetempat")) {
				table = "dbo.area_satuan_lingkungan_setempat";
				df = spark.read().jdbc(url, table, properties);
			}
			
			if(headers.equals(true)) {
				var header = df.columns();
				
				String columns = "{\"data\":\"" + "No" + "\",\"defaultContent\":" + "\"\"," + "\"name\":\"" + "No" + "\"}";
				
				for (int i = 0; i<header.length; i++){
					columns = columns + ",{\"data\":\"" + header[i] + "\",\"defaultContent\":" + "\"\"," + "\"name\":\"" + header[i] + "\"}";
				}
				
				result = "{\"columns\":[" + columns + "],\"status\":\"succeed\"}";
			} else {
				Integer draw = paramsObject.get("draw").getAsInt();
				Integer length = paramsObject.get("length").getAsInt();
				Integer start = paramsObject.get("start").getAsInt();

				df.createOrReplaceTempView("df");
				
				Dataset<Row> results = spark.sql("SELECT * FROM df");
				String[] column = results.columns();
				results = results.withColumn("No", functions.row_number().over(Window.orderBy(column[0].toString())));
				
				Long total = results.count();
				
				String condition = "No >" + start;
				results = results.where(condition);
				results = results.limit(length);

				List<String> paginatedResults = results.toJSON().collectAsList();
				
				result = "{" + 
							"\"draw\":" + draw + 
							",\"recordsTotal\":" + total +
							",\"recordsFiltered\":" + total +
							",\"data\":" + paginatedResults +
							",\"status\":\"succeed\""+
						"}";
			}
		} catch(Exception e) {
			result = "{\"status\":\"failed\"}";
		}
		
		spark.stop();
		return result;
	};
	
	public static Route explorasi = (Request request, Response response) -> {
		response.type("application/json");
		response.header("Access-Control-Allow-Origin", "*");
		
		String params = request.body();
		
		JsonParser parser = new JsonParser();
		JsonElement paramsJson = parser.parse(params);
		
		JsonObject paramsObject = paramsJson.getAsJsonObject();
		Boolean headers = paramsObject.get("headers").getAsBoolean();
		JsonArray dataset = paramsObject.get("dataframe").getAsJsonArray();
		JsonArray explorasi = paramsObject.get("explorasi").getAsJsonArray();
		
		String result = null;
		try {
			var spark = SparkSession.builder().config(ApacheSparkHelper.getConfiguration()).getOrCreate();
			var url = "jdbc:sqlserver://localhost:1433";
			String table = null;
			
			var properties = new Properties();
			properties.put("user","sa");
			properties.put("password","Blackrose9");
			
			String path = "";
			Dataset<Row> df = null;
			for(int i = 0; i<dataset.size(); i++) {
				JsonObject jsonObject = dataset.get(i).getAsJsonObject();
				String dname = jsonObject.get("dataset").getAsString();
				
				if(dname.equals("rekap2018")) {
					path = "C:/Users/62856/Desktop/SHOPI/csv/rekap2018.csv";
					spark.read().format("CSV").option("inferSchema", "true").option("header", "true").load(path).createTempView(jsonObject.get("alias").getAsString());
				} 
				else if(dname.equals("masterBlokSensus")) {
					path = "C:/Users/62856/Desktop/SHOPI/csv/mstbs_20121_st2013_ok.csv";
					spark.read().format("CSV").option("inferSchema", "true").option("header", "true").load(path).createTempView(jsonObject.get("alias").getAsString());
				} 
				else if(dname.equals("hierarkiBlokSensus")) {
					path = "C:/Users/62856/Desktop/SHOPI/csv/hirarki_match_flag.csv";
					spark.read().format("CSV").option("inferSchema", "true").option("header", "true").load(path).createTempView(jsonObject.get("alias").getAsString());
				}  
				else if(dname.equals("blokSensusGabung")) {
					path = "C:/Users/62856/Desktop/SHOPI/csv/bs_gabung.csv";
					spark.read().format("CSV").option("inferSchema", "true").option("header", "true").load(path).createTempView(jsonObject.get("alias").getAsString());
				}
				else if(dname.equals("bisnisPerusahaan")) {
					table = "dbo.business_perusahaan";
					df = spark.read().jdbc(url, table, properties);
					df.createTempView(jsonObject.get("alias").getAsString());
				}
				else if(dname.equals("bisnisEmail")) {
					table = "dbo.business_alamat_email";
					df = spark.read().jdbc(url, table, properties);
					df.createTempView(jsonObject.get("alias").getAsString());
				}
				else if(dname.equals("bisnisFax")) {
					table = "dbo.business_alamat_fax";
					df = spark.read().jdbc(url, table, properties);
					df.createTempView(jsonObject.get("alias").getAsString());
				}
				else if(dname.equals("bisnisTelepon")) {
					table = "dbo.business_alamat_telepon";
					df = spark.read().jdbc(url, table, properties);
					df.createTempView(jsonObject.get("alias").getAsString());
				}
				else if(dname.equals("bisnisWeb")) {
					table = "dbo.business_alamat_web";
					df = spark.read().jdbc(url, table, properties);
					df.createTempView(jsonObject.get("alias").getAsString());
				}
				else if(dname.equals("bisnisAktivitasPerusahaan")) {
					table = "dbo.business_aktivitas_perushaan";
					df = spark.read().jdbc(url, table, properties);
					df.createTempView(jsonObject.get("alias").getAsString());
				}
				else if(dname.equals("bisnisKontakPerusahaan")) {
					table = "dbo.dbo.business_kontak_perusahaan";
					df = spark.read().jdbc(url, table, properties);
					df.createTempView(jsonObject.get("alias").getAsString());
				}
				else if(dname.equals("bisnisPemegangSaham")) {
					table = "dbo.business_pemegang_saham";
					df = spark.read().jdbc(url, table, properties);
					df.createTempView(jsonObject.get("alias").getAsString());
				}
				else if(dname.equals("bisnisProdukPerusahaan")) {
					table = "dbo.business_produk_perusahaan";
					df = spark.read().jdbc(url, table, properties);
					df.createTempView(jsonObject.get("alias").getAsString());
				}
				else if(dname.equals("peoplePenduduk")) {
					table = "dbo.people_penduduk";
					df = spark.read().jdbc(url, table, properties);
					df.createTempView(jsonObject.get("alias").getAsString());
				}
				else if(dname.equals("peoplePendudukDefacto")) {
					table = "dbo.people_penduduk_defacto";
					df = spark.read().jdbc(url, table, properties);
					df.createTempView(jsonObject.get("alias").getAsString());
				}
				else if(dname.equals("familyKeluarga")) {
					table = "dbo.family_keluarga";
					df = spark.read().jdbc(url, table, properties);
					df.createTempView(jsonObject.get("alias").getAsString());
				}
				else if(dname.equals("familyKeluargaDefacto")) {
					table = "dbo.family_keluarga_defacto";
					df = spark.read().jdbc(url, table, properties);
					df.createTempView(jsonObject.get("alias").getAsString());
				}
				else if(dname.equals("areaSnapshotWilayah")) {
					table = "dbo.area_snapshot_wilayah";
					df = spark.read().jdbc(url, table, properties);
					df.createTempView(jsonObject.get("alias").getAsString());
				}
				else if(dname.equals("areaProvinsi")) {
					table = "dbo.area_provinsi";
					df = spark.read().jdbc(url, table, properties);
					df.createTempView(jsonObject.get("alias").getAsString());
				}
				else if(dname.equals("areaKabupatenKota")) {
					table = "dbo.area_kabupaten_kota";
					df = spark.read().jdbc(url, table, properties);
					df.createTempView(jsonObject.get("alias").getAsString());
				}
				else if(dname.equals("areaKecamatan")) {
					table = "dbo.area_kecamatan";
					df = spark.read().jdbc(url, table, properties);
					df.createTempView(jsonObject.get("alias").getAsString());
				}
				else if(dname.equals("areaKeluarahanDesa")) {
					table = "dbo.area_kelurahan_desa";
					df = spark.read().jdbc(url, table, properties);
					df.createTempView(jsonObject.get("alias").getAsString());
				}
				else if(dname.equals("areaBlokSensus")) {
					table = "dbo.area_blok_sensus";
					df = spark.read().jdbc(url, table, properties);
					df.createTempView(jsonObject.get("alias").getAsString());
				}
				else if(dname.equals("areaSatuanLingkunganSetempat")) {
					table = "dbo.area_satuan_lingkungan_setempat";
					df = spark.read().jdbc(url, table, properties);
					df.createTempView(jsonObject.get("alias").getAsString());
				}
			}
			
			for(int j = 0; j<explorasi.size(); j++) {
				JsonObject explorasiObject = explorasi.get(j).getAsJsonObject();
				String query = explorasiObject.get("query").getAsString();
				String alias2 = explorasiObject.get("alias2").getAsString();
				df = spark.sql(query);
				df.createOrReplaceTempView(alias2);
			}
			
			if(headers.equals(true)) {
				var header = df.columns();
				
				String columns = "{\"data\":\"" + "No" + "\",\"defaultContent\":" + "\"\"," + "\"name\":\"" + "No" + "\"}";
				
				for (int i = 0; i<header.length; i++){
					columns = columns + ",{\"data\":\"" + header[i] + "\",\"defaultContent\":" + "\"\"," + "\"name\":\"" + header[i] + "\"}";
				}
				
				result = "{\"columns\":[" + columns + "],\"status\":\"succeed\"}";
				
				spark.stop();
			} else {
				Integer draw = paramsObject.get("draw").getAsInt();
				Integer length = paramsObject.get("length").getAsInt();
				Integer start = paramsObject.get("start").getAsInt();
				
				df = df.limit(300);
				
				String[] column = df.columns();
				df = df.withColumn("No", functions.row_number().over(Window.orderBy(column[0].toString())));
				
				Long total = df.count();
				
				String condition = "No >" + start;
				df = df.where(condition);
				df = df.limit(length);

				List<String> paginatedResults = df.toJSON().collectAsList();
				
				spark.stop();
				
				result ="{" + 
							"\"draw\":" + draw + 
							",\"recordsTotal\":" + total +
							",\"recordsFiltered\":" + total +
							",\"data\":" + paginatedResults +
							",\"status\":\"succeed\""+
						"}";
			}
		} catch(Exception e) {
			result = "{\"status\":\"failed\"}";
		}
		
		return result;
	};
}
