package main;

import controller.ApiConfiguration;
import controller.DataController;
import controller.FrsController;
import transformer.JsonTransformer;

import static spark.Spark.*;

import org.apache.spark.SparkConf;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import lombok.var;

public class Main {
	public static void main(String[] arg) {
		SparkRunner sparkRunner = new SparkRunner();
        Thread t = new Thread(sparkRunner);
        t.start();
	}
	
	static class SparkRunner implements Runnable {


        public SparkRunner() {
        }

        public void run() {
            port(ApiConfiguration.instance().getPort());
            path("api/v1", () -> {
            	path("/frs", () -> {
            		path("/framing", () -> {
            			post("/datagui", "application/json", DataController.datagui);
            			post("/dataset", "application/json", DataController.dataset);
            			post("/explorasi", "application/json", DataController.explorasi);
            			post("/explorasi-save", "application/json", DataController.explorasiSave);
            		});
            	});   
            });

        }
    }
}
