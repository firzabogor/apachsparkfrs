package viewmodels;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DatasetViewModel {
	private String programName;
	private String cycleName;
	private String datasetName;
}
