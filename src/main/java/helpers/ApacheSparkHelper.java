package helpers;

import java.io.IOException;
import java.util.UUID;
import org.apache.spark.SparkConf;
import org.apache.spark.sql.SparkSession;

import controller.ApiConfiguration;

import org.apache.hadoop.security.UserGroupInformation;
import org.apache.hadoop.security.Credentials;
import org.apache.spark.deploy.SparkHadoopUtil;

import lombok.var;

public class ApacheSparkHelper {
	public static SparkConf getConfiguration() {
		var apacheSparkSettings = ApiConfiguration.instance().getApacheSparkSettings();
		var master = apacheSparkSettings.getMaster();
		var conf = new SparkConf().setAppName("dms-api").setMaster("local");
		return conf;
	}
}
